if (Number(process.version.slice(1).split(".")[0]) < 8) throw new Error("Node 8.0.0 or higher is required. Update Node on your system.");

const { Client } = require('discord.js');
const { promisify } = require('util');
const readdir = promisify(require('fs').readdir);
const Enmap = require('enmap');
const config = require('./config.js')
const chalk = require('chalk');
const { GiveawaysManager } = require('discord-giveaways');
require('dotenv-flow').config();
global.fetch = require('node-fetch')

const client = new Client({
	disableEveryone:  true,
	messageCacheMaxSize: 500,
	messageCacheLifetime: 120,
	messageSweepInterval: 60,
	messageEditHistoryMaxSize: 1000,
	intents: [Intents.FLAGS.GUILD_MESSAGES, Intents.FLAGS.GUILD_PRESENCES, Intents.FLAGS.GUILD_MEMBERS, Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_BANS, Intents.FLAGS.GUILD_WEBHOOKS, Intents.FLAGS.GUILD_INVITES, Intens.FLAGS.GUILD_MESSAGE_REACTIONS],
	//allowedMentions: ['roles', 'users'],
	partials: ['MESSAGE', 'CHANNEL', 'REACTION']
});

client.giveawaysManager = new GiveawaysManager(client, {
    storage: "./giveaways.json",
    updateCountdownEvery: 60000,
	hasGuildMembersIntent: true,
    default: {
        botsCanWin: false,
        exemptPermissions: [],
        embedColor: "#D900FF",
        reaction: "🎁",
		lastChance: 
		{
			enabled: true,
			content: '⚠️ **LAST CHANCE TO ENTER !** ⚠️',
			threshold: 5000,
			embedColor: '#FF0000'
		}
    }
});

client.commands = new Enmap();
client.aliases = new Enmap();
client.categories = new Enmap();
client.categories.fun = new Enmap();
client.categories.moderation = new Enmap();
client.categories.info = new Enmap();
client.categories.level = new Enmap();
client.categories.settings = new Enmap();
client.categories.giveaway = new Enmap();

client.logger = require('./utils/logger');

require('./utils/functions')(client);

const init = async () => {

	const cmdFiles = await readdir('./commands/');
	cmdFiles.forEach(f => {
		if (!f.endsWith('.js')) return;
		const response = client.loadCommand(f);
		if (response) {
			client.logger.log(response);
			//client.channels.cache.get('875677287058636800').send(response);
		}
	});
	console.log(`Loading a total of ${cmdFiles.length} commands.`);
	//client.channels.cache.get('875677287058636800').send(`Loading a total of ${cmdFiles.length} commands.`);

	const evtFiles = await readdir('./events/');
	evtFiles.forEach(f => {
		const evtName = f.split('.')[0];
		console.log(`Loading Event: ${evtName} 👌`);
		//client.channels.cache.get('875677287058636800').send(`Loading Event: ${evtName} 👌`);
		const event = require(`./events/${f}`);
		client.on(evtName, event.bind(null, client));
	});
	console.log(`Loading a total of ${evtFiles.length} events.`);
	//client.channels.cache.get('875677287058636800').send(`Loading a total of ${evtFiles.length} events.`);
	
	const logFiles = await readdir('./logging/');
	logFiles.forEach(f => {
		const logName = f.split('.')[0];
		console.log(`Loading Log: ${logName}`);
		//client.channels.cache.get('875677287058636800').send(`Loading Log: ${logName}`);
		const log = require(`./logging/${f}`);
		client.on(logName, log.bind(null, client));
	});
	console.log(`Loading a total of ${logFiles.length} Logging Functions.`);
	//client.channels.cache.get('875677287058636800').send(`Loading a total of ${logFiles.length} Logging Functions.`);

	client.login(`${config.token}`);
};

init();
