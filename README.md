[![Discord](https://img.shields.io/discord/595881103672475665?logo=Discord&style=for-the-badge)](https://discord.gg/eRPsZns)
# JoiBoi
JoiBoi is a multipurpose Discord Bot

[![Discord Bots](https://top.gg/api/widget/596765919297011736.svg)](https://top.gg/bot/596765919297011736) [![DigitalOcean Referral Badge](https://web-platforms.sfo2.cdn.digitaloceanspaces.com/WWW/Badge%201.svg)](https://www.digitalocean.com/?refcode=4c04ef842cc0&utm_campaign=Referral_Invite&utm_medium=Referral_Program&utm_source=badge)

## About

* __Author__: Lockyz Dev
* __Version__: 2.0.0
* __Default Prefix__: ?
* __Invite__: https://lockyzdev.net/joiboi/invite

## Modules
**These commands are subject to change.**

__Module__|__Info__|__Permission Level__
:--:|:--:|:--:
Fun|Various Fun Commands|Everyone
Moderation|Commands used by moderators and admins alike|Admins, Owners, MODERATOR SUPPORT COMING SOON
Giveaway|Wanna give stuff away? Use some of these|Admins, Owners
Information|Just general information stuffs|Everyone
Level|Speaketh and get levels.|Everyone
Settings|Enable/Disable various features within the bot.|Owners, ADMIN SUPPORT COMING SOON
