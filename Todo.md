# JoiBoi Todo List
## Next Update 24082021
### Additions
- [ ] Basic Spam Prevention *Toggleable through the settings command*
- [ ] Leave Notifications *Toggleable through the settings command*
- [ ] Mute Command
- [ ] Moderator Role Permissions
- [ ] Ticket System
- [ ] Self Assign Role Command *Will require it's own Table*
- [ ] Bot news at bottom of command outputs
- [ ] Allow Welcome Message to be changed
- [ ] Allow server owners/admins to choose what logs are created
- [ ] Invite Tracking
- [ ] Settings system uses slash commands
- [ ] Move all commands to slash commands
- [ ] Role Rewards added to Leveling System
- [ ] Auto-ban Feature *Based off of auto-ban database*

### Changes
None ATM

### Fixes
None ATM

## Planned ETA
I plan on getting all changes done BEFORE Quarter 4 of 2021. Please remember that there is still a possibility that this won't happen.

## Beta Testing
You can beta Test all features with [JoiBoi Canary](https://lockyzdev.net/joiboi/canary/invite/)
