require('dotenv-flow').config();

module.exports = {
    version: "13082021",
    prefix: "!",
    supportS: "https://discord.gg/eRPsZns",
    bName: "JoiBoi Canary",
    base: "2.0.0 custom",
    dev: "Lochlan Painter#6969",
    commby: "Lockyz",
    description: "JoiBoi is a multipurpose Discord Bot\nThis bot is in Beta. Be aware of bugs.\nCreated by Lockyz Dev <https://lockyzdev.net>\n\nPrefix = !\n\nInvite: <https://lockyzdev.net/joiboi/canary/invite>",
    embedColor: "00000",
};